package com.hillel.evo.adviser.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
public class UserCarEventDto {

    private Long id;

    private SimpleUserDto simpleUser;

    private Integer releaseYear;

    private CarModelDto carModel;

    private Set<ImageDto> images;
}
