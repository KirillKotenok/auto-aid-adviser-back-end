package com.hillel.evo.adviser.controller;

import com.hillel.evo.adviser.dto.EventBusinessDto;
import com.hillel.evo.adviser.dto.EventCreateDto;
import com.hillel.evo.adviser.dto.EventShortDto;
import com.hillel.evo.adviser.dto.EventUserDto;
import com.hillel.evo.adviser.exception.EventException;
import com.hillel.evo.adviser.service.EventService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.time.YearMonth;
import java.util.List;

@RestController
@RequestMapping("/scheduler")
@RequiredArgsConstructor
public class SchedulerController {

    private static final String ROLE_BUSINESS = "ROLE_BUSINESS";
    private static final String ROLE_USER = "ROLE_USER";

    @Qualifier("default")
    private final EventService eventService;

    @Secured({ROLE_BUSINESS, ROLE_USER})
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/{business-id}/{year-month}")
    public List<EventShortDto> getMonthlySchedulerByBusinessId(
            @PathVariable("business-id") Long businessId, @PathVariable("year-month") YearMonth yearMonth) {
        return eventService.getMonthlyEventsShort(yearMonth, businessId);
    }

    @Secured(ROLE_BUSINESS)
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/business/{business-id}/{year-month}")
    public List<EventBusinessDto> getMonthlySchedulerForBusiness(
            @PathVariable("business-id") Long businessId, @PathVariable("year-month") YearMonth yearMonth) {
        return eventService.getMonthlyBusinessEvents(yearMonth, businessId);
    }

    @Secured(ROLE_USER)
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/user/{user-id}/{year-month}")
    public List<EventUserDto> getMonthlySchedulerForUser(
            @PathVariable("user-id") Long userId, @PathVariable("year-month") YearMonth yearMonth) {
        return eventService.getMonthlyUserEvents(yearMonth, userId);
    }

    @Secured(ROLE_USER)
    @PostMapping(path = "/{business-id}",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity createEvent(
            @PathVariable("business-id") Long businessId, @RequestBody EventCreateDto createDto){
        try {
            return new ResponseEntity<>(eventService.createEvent(businessId, createDto), HttpStatus.CREATED);
        }catch (EventException ex) {
            return ResponseEntity.badRequest().body(ex.getMessage());
        }
    }

    @Secured(ROLE_BUSINESS)
    @ResponseStatus(HttpStatus.OK)
    @PatchMapping("/confirm/{event-id}")
    public void confirmEvent(@PathVariable("event-id") Long eventId) {
        eventService.confirmEvent(eventId);
    }

    @Secured(ROLE_BUSINESS)
    @ResponseStatus(HttpStatus.OK)
    @PatchMapping("/complete/{event-id}")
    public void completeEvent(@PathVariable("event-id") Long eventId) {
        eventService.completeEvent(eventId);
    }

    @Secured(ROLE_BUSINESS)
    @ResponseStatus(HttpStatus.OK)
    @PatchMapping("/decline/{event-id}")
    public void declineEvent(@PathVariable("event-id") Long eventId) {
        eventService.declineEvent(eventId);
    }
}
