package com.hillel.evo.adviser.enums;

public enum  EventStatus {
    IN_PROGRESS,
    ACCEPTED,
    COMPLETED,
    DECLINED
}
