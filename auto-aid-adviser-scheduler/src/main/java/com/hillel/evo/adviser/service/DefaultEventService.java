package com.hillel.evo.adviser.service;

import com.hillel.evo.adviser.dto.EventBusinessDto;
import com.hillel.evo.adviser.dto.EventCreateDto;
import com.hillel.evo.adviser.dto.EventShortDto;
import com.hillel.evo.adviser.dto.EventUserDto;
import com.hillel.evo.adviser.entity.Event;
import com.hillel.evo.adviser.entity.ServiceForBusiness;
import com.hillel.evo.adviser.enums.EventStatus;
import com.hillel.evo.adviser.exception.EventException;
import com.hillel.evo.adviser.mapper.EventMapper;
import com.hillel.evo.adviser.repository.BusinessRepository;
import com.hillel.evo.adviser.repository.EventRepository;
import com.hillel.evo.adviser.repository.ServiceForBusinessRepository;
import com.hillel.evo.adviser.repository.UserCarRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.YearMonth;
import java.util.List;

@Service(value = "eventServiceDefault")
@RequiredArgsConstructor
public class DefaultEventService implements EventService{

    private final EventRepository eventRepository;

    private final BusinessRepository businessRepository;

    private final ServiceForBusinessRepository serviceRepository;

    private final UserCarRepository carRepository;

    private final EventMapper mapper;

    @Override
    @Transactional
    public EventUserDto createEvent(Long businessId, EventCreateDto createDto) throws EventException{
        Event event = new Event();
        carRepository.findByCarId(createDto.getCarId())
                .ifPresentOrElse(event::setCar, () -> {throw new EventException("User car not found");});
        businessRepository.findById(businessId)
                .ifPresentOrElse(event::setBusiness, () -> {throw new EventException("Business not found");});
        ServiceForBusiness service = serviceRepository.findById(createDto.getServiceId())
                .orElseThrow(() -> new EventException("Service not found"));
        Long durationWork = service.getDurationWork();
        LocalDate date = createDto.getDate();
        LocalTime startTime = createDto.getStartTime();
        LocalTime endTime = startTime.plusMinutes(durationWork);
        Event nearestEvent = eventRepository.findNearestEvent(businessId, startTime, date);
        if (nearestEvent != null) {
            LocalTime end = nearestEvent.getEndTime();
            if (endTime.compareTo(end) < 0) {
                throw new EventException("Event already exists");
            }
        }
        if (eventRepository.hasEvent(businessId, startTime, endTime, date)){
            throw new EventException("Event already exists");
        }
        event.setService(service);
        event.setDate(date);
        event.setStartTime(startTime);
        event.setEndTime(endTime);
        event.setStatus(EventStatus.IN_PROGRESS);
        return mapper.toUserDto(eventRepository.save(event));
    }

    @Override
    @Transactional
    public void confirmEvent(Long eventId) {
        eventRepository.getOne(eventId).setStatus(EventStatus.ACCEPTED);
    }

    @Override
    @Transactional
    public void completeEvent(Long eventId) {
        eventRepository.getOne(eventId).setStatus(EventStatus.COMPLETED);
    }

    @Override
    @Transactional
    public void declineEvent(Long eventId) {
        eventRepository.getOne(eventId).setStatus(EventStatus.DECLINED);
    }

    @Override
    @Transactional
    public List<EventShortDto> getMonthlyEventsShort(YearMonth yearMonth, Long businessId) {
        return mapper.toListShortDtos(eventRepository
                .findEventsByMonth(yearMonth.getMonthValue(), yearMonth.getYear(), businessId, EventStatus.DECLINED));
    }

    @Override
    @Transactional
    public List<EventBusinessDto> getMonthlyBusinessEvents(YearMonth yearMonth, Long businessId) {
        return mapper.toListBusinessDtos(eventRepository
                .findEventsByMonthAndBusiness(yearMonth.getMonthValue(), yearMonth.getYear(), businessId));
    }

    @Override
    @Transactional
    public List<EventUserDto> getMonthlyUserEvents(YearMonth yearMonth, Long userId) {
        return mapper.toListUserDtos(eventRepository
                .findEventsByMonthAndUser(yearMonth.getMonthValue(), yearMonth.getYear(), userId));// eventDtos;
    }

    @Override
    @Transactional
    public void deleteBusinessEvents(Long businessId) {
        eventRepository.deleteAllByBusinessId(businessId);
    }
}
