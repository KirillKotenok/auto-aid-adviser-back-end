package com.hillel.evo.adviser.mapper;

import com.hillel.evo.adviser.SchedulerApplication;
import com.hillel.evo.adviser.dto.BusinessDto;
import com.hillel.evo.adviser.dto.EventBusinessDto;
import com.hillel.evo.adviser.dto.EventShortDto;
import com.hillel.evo.adviser.dto.EventUserDto;
import com.hillel.evo.adviser.dto.ServiceForBusinessDto;
import com.hillel.evo.adviser.entity.Business;
import com.hillel.evo.adviser.entity.CarBrand;
import com.hillel.evo.adviser.entity.CarModel;
import com.hillel.evo.adviser.entity.Event;
import com.hillel.evo.adviser.entity.Image;
import com.hillel.evo.adviser.entity.ServiceForBusiness;
import com.hillel.evo.adviser.entity.SimpleUser;
import com.hillel.evo.adviser.entity.TypeCar;
import com.hillel.evo.adviser.entity.UserCar;
import com.hillel.evo.adviser.enums.EventStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {SchedulerApplication.class})
class EventMapperTest {

    private static Business business = new Business();
    private static BusinessDto businessDto = new BusinessDto();
    private static ServiceForBusiness serviceForBusiness = new ServiceForBusiness();
    private static ServiceForBusinessDto serviceForBusinessDto = new ServiceForBusinessDto();
    private static Event event = new Event();
    private static UserCar car = new UserCar();
    private static SimpleUser simpleUser = new SimpleUser();
    private static CarModel carModel = new CarModel();
    private static CarBrand carBrand = new CarBrand();
    private static TypeCar typeCar = new TypeCar();
    private static Set<Image> images = new HashSet<>();
    private static Image image = new Image();
    private static List<Event> events;

    @Autowired
    private EventMapper mapper;

    @BeforeEach
    void setUp() {
        carModel.setCarBrand(carBrand);
        carModel.setTypeCar(typeCar);
        images.add(image);
        car.setSimpleUser(simpleUser);
        car.setCarModel(carModel);
        car.setImages(images);
        business.setId(1L);
        businessDto.setId(1L);
        serviceForBusiness.setId(1L);
        serviceForBusinessDto.setId(1L);
        event.setCar(car);
        event.setBusiness(business);
        event.setService(serviceForBusiness);
        event.setDate(LocalDate.now());
        event.setStartTime(LocalTime.now());
        event.setEndTime(LocalTime.now().plusHours(1L));
        event.setStatus(EventStatus.IN_PROGRESS);
        events = new ArrayList<>();
        events.add(event);
    }

    @Test
    void whenToUserDtoSetNullReturnNull() {
        assertNull(mapper.toUserDto(null));
    }

    @Test
    void whenToListBusinessDtosSetNullReturnNull() {
        assertNull(mapper.toListBusinessDtos(null));
    }

    @Test
    void whenToListUserDtosSetNullReturnNull() {
        assertNull(mapper.toListUserDtos(null));
    }

    @Test
    void whenToListShortDtosSetNullReturnNull() {
        assertNull(mapper.toListShortDtos(null));
    }

    @Test
    void whenToBusinessDtoSetEmptyFieldsReturnEmptyDto2() {
        //given
        images.add(null);
        car.setImages(images);
        car.setCarModel(null);
        car.setSimpleUser(null);
        event.setCar(car);
        events.add(event);
        //when
        List<EventBusinessDto> eventDtos = mapper.toListBusinessDtos(events);
        //then
        assertNull(eventDtos.get(1).getCar().getReleaseYear());
    }

    @Test
    void whenToBusinessDtoSetEmptyImageReturnEmptyDto3() {
        //given
        car.setImages(null);
        carModel.setCarBrand(null);
        carModel.setTypeCar(null);
        car.setCarModel(carModel);
        event.setCar(car);
        events.add(event);
        //when
        List<EventBusinessDto> eventDtos = mapper.toListBusinessDtos(events);
        //then
        assertNull(eventDtos.get(1).getCar().getCarModel().getCarBrand());
        assertNull(eventDtos.get(1).getCar().getCarModel().getTypeCar());
    }

    @Test
    void whenToUserDtoSetEmptyEntityReturnEmptyDto() {
        //given
        Event event = new Event();
        //when
        EventUserDto eventDto = mapper.toUserDto(event);
        //then
        assertNull(eventDto.getBusiness());
        assertNull(eventDto.getStartTime());
    }

    @Test
    void whenToListBusinessDtoSetEmptyEntityReturnEmptyDto() {
        //given
        event.setCar(null);
        event.setBusiness(null);
        event.setService(null);
        event.setDate(null);
        event.setStartTime(null);
        event.setEndTime(null);
        event.setStatus(null);
        events.add(event);
        //when
        List<EventBusinessDto> eventDto = mapper.toListBusinessDtos(events);
        //then
        assertNull(eventDto.get(1).getCar());
        assertNull(eventDto.get(1).getStartTime());
    }

    @Test
    void whenToListUserDtosSetEmptyEntityReturnEmptyDto() {
        //given
        event.setCar(null);
        event.setBusiness(null);
        event.setService(null);
        event.setDate(null);
        event.setStartTime(null);
        event.setEndTime(null);
        event.setStatus(null);
        events.add(event);
        //when
        List<EventUserDto> eventDtos = mapper.toListUserDtos(events);
        //then
        assertNull(eventDtos.get(1).getBusiness());
        assertNull(eventDtos.get(1).getDate());
    }

    @Test
    void whenToListShortDtosSetEmptyEntityReturnEmptyDto() {
        //given
        event.setCar(null);
        event.setBusiness(null);
        event.setService(null);
        event.setDate(null);
        event.setStartTime(null);
        event.setEndTime(null);
        event.setStatus(null);
        events.add(event);
        //when
        List<EventShortDto> eventDtos = mapper.toListShortDtos(events);
        //then
        assertNull(eventDtos.get(1).getStartTime());
        assertNull(eventDtos.get(1).getEndTime());
    }

    @Test
    void whenToUserDtoSetEventReturnEventDto() {
        //when
        EventUserDto eventDto = mapper.toUserDto(event);
        //then
        assertEquals(event.getBusiness(), business);
        assertEquals(event.getStartTime(), eventDto.getStartTime());
        assertEquals(event.getStatus(), eventDto.getStatus());
    }

    @Test
    void whenToListBusinessDtosSetEventReturnEventDto() {
        //when
        List<EventBusinessDto> eventDtos = mapper.toListBusinessDtos(events);
        //then
        assertEquals(event.getDate(), eventDtos.get(0).getDate());
        assertEquals(event.getStartTime(), eventDtos.get(0).getStartTime());
        assertEquals(event.getStatus(), eventDtos.get(0).getStatus());
    }

    @Test
    void whenToListUserDtosSetEventReturnEventDto() {
        //when
        List<EventShortDto> eventDtos = mapper.toListShortDtos(events);
        //then
        assertEquals(event.getDate(), eventDtos.get(0).getDate());
        assertEquals(event.getStartTime(), eventDtos.get(0).getStartTime());
    }

    @Test
    void whenToListShortDtosSetEventReturnEventDto() {
        //when
        List<EventUserDto> eventDtos = mapper.toListUserDtos(events);
        //then
        assertEquals(event.getDate(), eventDtos.get(0).getDate());
        assertEquals(event.getStartTime(), eventDtos.get(0).getStartTime());
        assertEquals(event.getStatus(), eventDtos.get(0).getStatus());
    }

}
