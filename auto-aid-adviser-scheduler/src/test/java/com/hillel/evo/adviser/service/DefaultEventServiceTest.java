package com.hillel.evo.adviser.service;

import com.hillel.evo.adviser.SchedulerApplication;
import com.hillel.evo.adviser.dto.EventBusinessDto;
import com.hillel.evo.adviser.dto.EventCreateDto;
import com.hillel.evo.adviser.dto.EventUserDto;
import com.hillel.evo.adviser.dto.EventShortDto;
import com.hillel.evo.adviser.entity.Event;
import com.hillel.evo.adviser.enums.EventStatus;
import com.hillel.evo.adviser.exception.EventException;
import com.hillel.evo.adviser.repository.EventRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.YearMonth;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@SpringBootTest(classes = {SchedulerApplication.class, TestDatabaseService.class})
@Sql(value = {"/data-h2-test.sql"},
        executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class DefaultEventServiceTest {

    @Autowired
    @Qualifier(value = "eventServiceDefault")
    private EventService eventService;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private TestDatabaseService dbService;

    private static EventCreateDto createDto;
    private static LocalTime startTime;
    private static LocalDate date;
    private static Long businessId;
    private static Long eventId;
    private static Long userId;

    @BeforeEach
    void setUp() {
        startTime = LocalTime.now();
        date = LocalDate.now();
        Long carId = dbService.getId("user_car", "release_year", "2016");
        Long serviceId = dbService.getId("service", "name", "straightening dents");
        businessId = dbService.getId("business", "name", "user 1 STO 1");
        eventId = dbService.getId("event", "car_id", String.valueOf(carId));
        userId = dbService.getId("adviser_usr", "email", "svg@mail.com");
        createDto = new EventCreateDto();
        createDto.setCarId(carId);
        createDto.setDate(LocalDate.now());
        createDto.setServiceId(serviceId);
        createDto.setStartTime(LocalTime.now());
    }

    @AfterEach
    void truncate() {
        dbService.truncate();
    }

    @Test
    void whenCreateEventShouldCreateIt() {
        //when
        EventUserDto eventDto = eventService.createEvent(businessId, createDto);
        //then
        assertEquals(date, eventDto.getDate());
        assertEquals(EventStatus.IN_PROGRESS, eventDto.getStatus());
    }

    @Test
    void whenCreateEventShouldThrowCarNotFound() {
        //given
        createDto.setCarId(999L);
        //then
        assertThrows(EventException.class, () -> eventService.createEvent(businessId, createDto));
    }

    @Test
    void whenCreateEventShouldThrowBusinessNotFound() {
        assertThrows(EventException.class, () -> eventService.createEvent(999L, createDto));
    }

    @Test
    void whenCreateEventShouldThrowServiceNotFound() {
        //when
        createDto.setServiceId(999L);
        //then
        assertThrows(EventException.class, () -> eventService.createEvent(businessId, createDto));
    }

    @Test
    void whenCreateEventShouldThrowEventExistsInnerRange() {
        //given
        startTime = LocalTime.of(10,20);
        date = LocalDate.of(2019, 12, 26);
        createDto.setStartTime(startTime);
        createDto.setDate(date);
        //then
        assertThrows(EventException.class, () -> eventService.createEvent(businessId, createDto));
    }

    @Test
    void whenCreateEventShouldThrowEventExistsOuterRange() {
        //given
        startTime = LocalTime.of(10,55,50,297);
        date = LocalDate.of(2019, 12, 26);
        createDto.setStartTime(startTime);
        createDto.setDate(date);
        //then
        assertThrows(EventException.class, () -> eventService.createEvent(businessId, createDto));
    }

    @Test
    @Transactional
    void whenConfirmEventShouldConfirmIt() {
        //when
        eventService.confirmEvent(eventId);
        Event eventConfirm = eventRepository.getOne(eventId);
        //then
        assertEquals(EventStatus.ACCEPTED, eventConfirm.getStatus());
    }

    @Test
    @Transactional
    void whenCompleteEventShouldConfirmIt() {
        //when
        eventService.completeEvent(eventId);
        Event eventComplete = eventRepository.getOne(eventId);
        //then
        assertEquals(EventStatus.COMPLETED, eventComplete.getStatus());
    }

    @Test
    @Transactional
    void whenDeclineEventShouldDeclineIt() {
        //when
        eventService.declineEvent(eventId);
        Event eventDecline = eventRepository.getOne(eventId);
        //then
        assertEquals(EventStatus.DECLINED, eventDecline.getStatus());
    }

    @Test
    void whenGetMonthlyEventsShortShouldReturnThem() {
        //when
        List<EventShortDto> shortDtos = eventService.getMonthlyEventsShort(YearMonth.of(2019,12), businessId);
        //then
        assertEquals(5, shortDtos.size());
    }

    @Test
    void whenGetMonthlyEventsByBusinessShouldReturnThem() {
        //when
        List<EventBusinessDto> businessEvents = eventService.getMonthlyBusinessEvents(YearMonth.of(2019,12), businessId);
        //then
        assertEquals(7, businessEvents.size());
    }

    @Test
    void whenGetMonthlyEventsByUserShouldReturnThem() {
        //when
        List<EventUserDto> userEvents = eventService.getMonthlyUserEvents(YearMonth.of(2019,12),userId);
        //then
        assertEquals(1, userEvents.size());
    }

    @Test
    void whenDeleteBusinessEventsShouldDeleteThem() {
        //when
        eventService.deleteBusinessEvents(businessId);
        List<Event> events = eventRepository.findAllByBusiness(businessId);
        //then
        assertEquals(Collections.EMPTY_LIST, events);
    }
}
