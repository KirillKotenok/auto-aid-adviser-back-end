package com.hillel.evo.adviser.dto;

import com.hillel.evo.adviser.enums.EventStatus;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
@NoArgsConstructor
public class EventUserDto {

    private Long id;

    private BusinessShortDto business;

    private ServiceForBusinessShortDto service;

    private LocalDate date;

    private LocalTime startTime;

    private LocalTime endTime;

    private EventStatus status;
}
