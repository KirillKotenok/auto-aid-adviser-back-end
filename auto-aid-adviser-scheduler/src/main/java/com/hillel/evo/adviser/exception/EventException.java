package com.hillel.evo.adviser.exception;

public class EventException extends RuntimeException {
    static final long serialVersionUID = 1L;

    public EventException(String message) {
        super(message);
    }
}
