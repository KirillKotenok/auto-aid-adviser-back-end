-- user
insert into adviser_usr( email, password, role, active, activation_code) values
('svg@mail.com', '12345', 'ROLE_USER', true, null),
('bvg@mail.com', '12345', 'ROLE_BUSINESS', true, null);

insert into simple_usr( first_name, last_name, user_details_id) values
('Vasya', 'Gogy', (select id from adviser_usr where email like 'svg@mail.com'));

insert into business_usr(user_details_id) values
((select id from adviser_usr where email like 'bvg@mail.com'));


-- car
insert into type_car(name) values ('coupe'), ('crossover');

insert into car_brand(name) values ('Audi'), ('BMW');

insert into car_model(name, car_brand_id, type_car_id) values
    ('M5', (select id from car_brand where name like 'BMW'), (select id from type_car where name like 'crossover'));

insert into user_car(release_year, car_model_id, simple_user_user_details_id) values
('2016', (select id from car_model where name like 'M5'), (select user_details_id from simple_usr where first_name in('Vasya'))),
('2017', null, null);


-- image
insert into image(key_file_name, original_file_name, user_car_id) values
('3/1/00b8c076-68bd-42fd-ba3f-dadd398f838c-bg.jpg', '00b8c076-68bd-42fd-ba3f-dadd398f838c-bg.jpg', (select  id from user_car where release_year like '2016'));


-- business
insert into business_type(name) values
('CTO');

insert into service_type(name, business_type_id) values
('body', (select id from business_type where name like 'CTO'));

insert into service(name, duration_work, service_type_id) values
('straightening dents', 30, (select id from service_type where name like 'body'));

insert into business(phone, address, latitude, longitude, name, business_user_user_details_id) values
('098-123-45-67', 'Kiev', 100, 100, 'user 1 STO 1', (select id from adviser_usr a inner join business_usr b on (a.id = b.user_details_id) where a.email like 'bvg@mail.com'));

insert into business_has_service(business_id, service_for_businesses_id)
select b.id, s.id from business b, service s;

insert into work_time(day, from_time, to_time, business_id) values
(0, now(), now(), (select id from business limit 1));


--event
insert into event (date, start_time, end_time, status, business_id, service_id, car_id) values
('2019-12-26', '10:15:50.297', '11:15:50.297', 'IN_PROGRESS', (select id from business where name like 'user 1 STO 1'), (select id from service where name like 'straightening dents'), (select id from user_car where release_year like '2016')),
('2019-12-26', '12:15:50.297', '13:15:50.297', 'ACCEPTED', (select id from business where name like 'user 1 STO 1'), (select id from service where name like 'straightening dents'), (select id from user_car where release_year like '2017')),
('2019-12-26', '13:15:50.297', '14:15:50.297', 'DECLINED', (select id from business where name like 'user 1 STO 1'), (select id from service where name like 'straightening dents'), (select id from user_car where release_year like '2018')),
('2019-12-26', '14:15:50.297', '15:15:50.297', 'COMPLETED', (select id from business where name like 'user 1 STO 1'), (select id from service where name like 'straightening dents'), (select id from user_car where release_year like '2015')),
('2019-12-27', '23:15:50.297', '21:15:50.297', 'DECLINED', (select id from business where name like 'user 1 STO 1'), (select id from service where name like 'straightening dents'), (select id from user_car where release_year like '2014')),
('2019-12-28', '23:15:50.297', '21:15:50.297', 'ACCEPTED', (select id from business where name like 'user 1 STO 1'), (select id from service where name like 'straightening dents'), (select id from user_car where release_year like '2013')),
('2019-12-29', '23:15:50.297', '21:15:50.297', 'IN_PROGRESS', (select id from business where name like 'user 1 STO 1'), (select id from service where name like 'straightening dents'), (select id from user_car where release_year like '2012'));