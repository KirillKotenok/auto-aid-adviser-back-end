package com.hillel.evo.adviser.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
@NoArgsConstructor
public class EventShortDto {

    private Long id;

    private LocalDate date;

    private LocalTime startTime;

    private LocalTime endTime;
}
