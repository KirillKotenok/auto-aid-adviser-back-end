package com.hillel.evo.adviser.mapper;

import com.hillel.evo.adviser.dto.EventBusinessDto;
import com.hillel.evo.adviser.dto.EventShortDto;
import com.hillel.evo.adviser.dto.EventUserDto;
import com.hillel.evo.adviser.entity.Event;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring",
        uses = {UserCarMapper.class,
                BusinessMapper.class,
                ServiceForBusinessMapper.class
})
public interface EventMapper {

    EventUserDto toUserDto(Event event);

    List<EventBusinessDto> toListBusinessDtos(List<Event> events);

    List<EventUserDto> toListUserDtos(List<Event> events);

    List<EventShortDto> toListShortDtos(List<Event> events);
}
